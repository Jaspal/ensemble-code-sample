const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: [
    'vuetify'
  ],
  chainWebpack: config => {
    config.performance
      .maxEntrypointSize(0x100000)
      .maxAssetSize(0x100000)
  }
})
