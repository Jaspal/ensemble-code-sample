
# Ensemble Code Sample

Ensemble Developer Code Sample Submission by Jaspal Singh



## Task
Implement an app that can search OMDB for movies. The app can be implemented in any of the following tech stacks
* Web
    
        o React
        o Angular
        o Vue

* Android

        o Native (Java or Kotlin)
        o React Native
        o Flutter

* iOS

        o Native
        o React Native
        o Flutter

## Technical Requirements
* Search results must come from OMDB API

    o Documentation: http://www.omdbapi.com/

    o Free API key: http://www.omdbapi.com/apikey.aspx

* Each search result should display poster, title, year of release and a button. Clicking the button should do nothing but it should support displaying a label
* Updates to the search term should update the result list

## OMDB API KEY: 74df1475
## Web app build for production: http://omdbsearch.infinityfreeapp.com/
![](./result.gif)
